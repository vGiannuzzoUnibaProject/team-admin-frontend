[![React](https://img.shields.io/badge/react-16.13.1-blue)]()


# Team Admin

Interfaccia web la dei log e degli utenti Teams,
## Prerequisiti

Installare le seguenti applicazioni

```bash
Node js
Yarn
React dev tools (extension for browser)
Redux dev tools (extension for browser)
```

Creare un applicazione su Azure Active Directory


## Download repository

```bash
git clone https://vGiannuzzoUnibaProject@bitbucket.org/vGiannuzzoUnibaProject/team-admin-frontend.git
```

## Installazione con yarn

```bash
yarn install
```

## Configurazione
Settare nel file src/helpers/baseUrl.js l'indirizzo ip del backend
```javascript
export const config  = {
      baseUrl: 'http://localhost:8080'

}
```

Settare nel file src/helpers/tenantMicrosoft.js l'accesso al token Microsoft
```javascript
export const tenantMicrosoft={
    clientId: "ca9d4d94-09d3-48a7-846a-0ba417d54f2e",
    withUserData: true,
    redirectUri:"http://localhost:3000",
    tenantUrl:"https://login.microsoftonline.com/c6328dc3-afdf-40ce-846d-326eead86d49",
    debug: true
}

```

## Avvio

```bash
yarn start
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
Uniba