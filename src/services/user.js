import { config } from '../helpers/baseUrl';
import { authHeader } from '../helpers/auth-header';
import { Redirect } from 'react-router-dom';
import { history }  from '../helpers/history';

export const userService = {
    login,
    logout,
    register,
    getAll,
    getById,
    update,
    delete: _delete,
    getRole
    
};

function getRole(){
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.baseUrl}/user/role`, requestOptions)
    .then(handleResponse)
    .then(response => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        return response.role;
        
    });


}
function login(username, password) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username, password })
    };

    return fetch(`${config.baseUrl}/login`, requestOptions)
        .then(handleResponse)
        .then(response => {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('user', JSON.stringify(response));
            return response;
            
        });
        
}

function logout() {
    // remove user from local storage to log user out
    let token=JSON.parse(localStorage.getItem('user'))
   
    if (token){ 
    const requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer '+token['token']},
    };
    return fetch(`${config.baseUrl}/logout`, requestOptions)
    .then(response => {
            if (response.ok){ 
            localStorage.removeItem('user')
            history.push('/admin/login');
        
            }else history.push('/admin/login');
            
        })
    }

}

function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.baseUrl}/users`, requestOptions).then(handleResponse);
}

function getById(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.baseUrl}/users/${id}`, requestOptions).then(handleResponse);
}

function register(user) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return fetch(`${config.baseUrl}/users/register`, requestOptions).then(handleResponse);
}

function update(user) {
    const requestOptions = {
        method: 'PUT',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return fetch(`${config.apiUrl}/users/${user.id}`, requestOptions).then(handleResponse);;
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/users/${id}`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
    return response.text().then(text => {

        const data = text && JSON.parse(text);

        if (!response.ok) {
            
            if (response.status == 401) {
                // auto logout if 401 response returned from api
                logout();
                
            }
            if( response.status == 400 ||response.status == 404 ){
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
            }
            if( response.status == 500){
                console.log("errore")
               
                    history.push('./error',response.status);
    
    
                }
            

            

        }

        return data;
    });
}

export default handleResponse;