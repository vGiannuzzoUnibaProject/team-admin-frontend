import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import rootReducer from './reducers';

//const loggerMiddleware = createLogger();
//const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
/*

composeEnhancer(applyMiddleware(
        thunkMiddleware,
        loggerMiddleware
    )


*/
export const store = createStore(
    rootReducer,
    applyMiddleware(
        thunkMiddleware
    )
    
);