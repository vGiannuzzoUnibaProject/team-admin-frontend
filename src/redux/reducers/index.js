import { combineReducers } from 'redux';

import { authentication } from './authentication';
import { registration } from './registration';
import { users } from './user';
//import { alert } from './alert.reducer';

const rootReducer = combineReducers({
    authentication,
    registration,
    users,
});

export default rootReducer;