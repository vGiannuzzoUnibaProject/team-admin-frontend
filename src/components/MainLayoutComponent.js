import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import "./MainLayout.css";
import { useDispatch, useSelector } from "react-redux";

//https://stackoverflow.com/questions/49046010/passing-props-in-protected-route-reactjs
import { Avatar, Layout, Menu, Breadcrumb, Dropdown,Image,Typography,Divider } from "antd";
import {
  DatabaseOutlined,
  UserOutlined,
  LogoutOutlined,
  DashboardOutlined,
  UnorderedListOutlined,
  InteractionOutlined,
  AuditOutlined 

} from "@ant-design/icons";
import user from "../static/user.png";
import logo from "../static/logo.png";
import { userActions } from "../redux/actions/user";

const { Title } = Typography;
const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

function MenuSider(props){
  console.log(props.role)
    if(props.role=="admin"){
      return (
        <Menu theme="dark"  defaultOpenKeys={["sub1", "sub2"]} mode="inline">
      <div className="logo-header">
      <Link to="/Home"><Image width={32} height={32} src={logo} />
        <h1 id={"title-logo"} >Teams Admin </h1>

      </Link>
      </div>
          <Menu.Item key="1"icon={<DatabaseOutlined />}>
            <Link to="/admin/logs">Logs di accesso</Link>
          </Menu.Item>
          <Menu.Item key="2"icon={<UnorderedListOutlined />}>
            <Link to="/admin/actions">Azioni Utenti</Link>
          </Menu.Item>
          <Menu.Item key="3"icon={<InteractionOutlined />}>
            <Link to="/admin/action_administrators">Azioni Administrator</Link>
          </Menu.Item>
          <Menu.Item key="4"icon={<DashboardOutlined />}>
            <Link to="/admin/dashboard">Dashboard</Link>
          </Menu.Item>
          <Menu.Item key="6"icon={<AuditOutlined />}>
            <Link to="/admin/courses">Corsi</Link>
          </Menu.Item>
          <Menu.Item key="5"icon={<UserOutlined />}>
            <Link to="/admin/users/manage">Utenti</Link>
          </Menu.Item>
      </Menu>
      )
    }else return (
      <Menu theme="dark"  defaultOpenKeys={["sub1", "sub2"]} mode="inline">
      <div className="logo-header">
      <Link to="/Home"><Image width={32} height={32} src={logo} />
        <h1 id={"title-logo"} >Teams Admin </h1>

      </Link>
      </div>
      <Menu.Item key="1"icon={<DatabaseOutlined />}>
            <Link to="/admin/logs">Logs di accesso</Link>
          </Menu.Item>
          <Menu.Item key="4"icon={<DashboardOutlined />}>
            <Link to="/admin/dashboard">Dashboard</Link>
          </Menu.Item>
          <Menu.Item key="6"icon={<AuditOutlined />}>
            <Link to="/admin/courses">Corsi</Link>
          </Menu.Item>
          
         
      </Menu>
    )
}

//Passare delle props
function SideMenu(props) {
  const [collpased, setCollapsed] = useState(false);
  
  
  
  return (
    <Sider
      collapsed={collpased}
      onCollapse={() => setCollapsed(!collpased)}
      style={{
        overflow: "auto",
        height: "100vh",
        position: "sticky",
        top: 0,
        left: 0,
      }}
    >
      <MenuSider role={props.role}/>
      
    </Sider>
  );
}

function HeaderBar(props) {
  return (
    <Header
      className="site-layout-background flex-header"
      style={{ padding: 0 }}
    >
      <Dropdown overlay={() => DropdownIcon(props)} placement="bottomCenter" arrow>
        <Avatar
          src={user}
          className="header-right"
          style={{ float: "right" }}
          icon={<UserOutlined />}
        />
      </Dropdown>
    </Header>
  );
}

//Passare delle props
function DropdownIcon(props) {
   
  const menu = (
    <Menu style={{cursor: "pointer"}}>
      <Menu.Item icon={<UserOutlined />}>
        <Link to="/admin/changepassword">Cambio Password</Link>
      </Menu.Item>
      <Menu.Item icon={<LogoutOutlined />} >
          <span onClick={props.props}>Logout</span>
        </Menu.Item>
    </Menu>
  );
  return menu;
}
function BreadcrumbRoute() {
  return (
    <div>
        
    </div>
    /* <Breadcrumb style={{ margin: '16px 0' }}>
        <Breadcrumb.Item>User</Breadcrumb.Item>
        <Breadcrumb.Item>Bill</Breadcrumb.Item>
    </Breadcrumb>
    */
  );
}

function MainLayout(props) {
  const ViewComponent = props.view;
  const dispatch = useDispatch();
  const onLogout = () =>dispatch(userActions.logout());
  
  const role = useSelector((state) => state.users.role);

    useEffect(() => {
      
      if(role==null || role !=""){ 
        console.log(role)
        dispatch(userActions.getRoleUser());

      }

  }, []);
  return (
    <Layout style={{ minHeight: "100vh" }}>
      <SideMenu role={role}/>
      <Layout className="site-layout">
        <HeaderBar  props={onLogout}/>
        <Content style={{ margin: "0 16px" }}>
          <BreadcrumbRoute view={props.view}/>
          {
            //<div className="site-layout-background" style={{ padding: 24, height:"79vh"}}>
          }
          <ViewComponent />
          {
            //</Content></div>
          }
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Teams Administrator 2021
        </Footer>
      </Layout>
    </Layout>
  );
}

export default MainLayout;
