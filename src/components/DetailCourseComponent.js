import { useLocation } from "react-router-dom";
import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { history }  from '../helpers/history';
import { requestMethod } from "../helpers/requestMethod";

import { getRequest } from "../helpers/getRequest";
import handleResponse from "../services/user";
import { config } from "../helpers/baseUrl";
import {
    Table,
    Tag,
    Modal,
    Switch,
    Button,
    Form,
    Input,
    Select,
    Alert,
    AutoComplete,
    Popconfirm,
    message,
    Result,
    PageHeader
  } from "antd";

function DetailCourse(props){
    const role = useSelector((state) => state.users.role);

    const location = useLocation();
    const id_courses = location.state  ;
    const [loading,setLoading]=useState(false)
    const [detailCourses,setDetailCourses]=useState([])
    const [coursesLabel,setCoursesLabel]=useState()
    const [modalEdit, setModalEdit] = useState(false);
    const [tmpId,setTmpId]=useState()
    const [tmpLabel,setTmpLabel]=useState()
    const [tmpUrl,setTmpUrl]=useState()
    const [tmpColor,setTmpColor]=useState()

    const [modalNew, setModalNew] = useState(false);


    useEffect(() => {
        async function load() {
          fetch(`${config.baseUrl}/courses/detail/id/${id_courses}`, getRequest(requestMethod.GET))
            .then(handleResponse)
            .then((response) => {
              setDetailCourses(response.result);
              setCoursesLabel(response.courses)
              setLoading(false);
            })
            .catch((error) => {
              console.log(error);
            });
        }
    
        load();
      }, []);

      const onFinishEdit = (values) =>{
        console.log(values)
        setModalEdit(false)
        fetch(
            `${config.baseUrl}/courses/detail/id/${tmpId}`,
            getRequest(requestMethod.PUT, JSON.stringify(values))
          )
            .then(handleResponse)
            .then(() => refreshDetail());
        }

      function refreshDetail() {
        setLoading(true);
        fetch(`${config.baseUrl}/courses/detail/id/${id_courses}`, getRequest(requestMethod.GET))
        .then(handleResponse)
          .then((response) => {
            setDetailCourses(response.result);
            setLoading(false);
          })
          .catch((error) => {
            console.log(error);
          });
      }

      function deleteCoursesDetail(id) {
          console.log(id)
        fetch(
          `${config.baseUrl}/courses/detail/id/${id}`,
          getRequest(requestMethod.DELETE)
        )
          .then(handleResponse)
          .then((response) => {
            refreshDetail();
          });
      }

      function editDetailCourses(object){
        setTmpId( object.id)
        setTmpLabel(object.label)
        setTmpUrl(object.url)
        setTmpColor(object.color)
        setModalEdit(true)
  
  
    }


    const dataSource = detailCourses;

  const columns = [
    {
      title: "Nome Link",
      dataIndex: "label",
    },
    {
    title: "Colore bottone",
    dataIndex: "color",
    render:(e,record) => {
        console.log(record)
        let color=""
        if(!record['color'])
            color="#white"
        else color=record['color']
        return (
            <div style={{backgroundColor: color,borderStyle:"solid",borderWidth: "thin",width: 100, height:20
        }}></div>
        )
    }
    
},
    {
        title: "Url",
        dataIndex: "url",
      },
      {
        title: "Azione",
        key: "action",
        render: (text, record) => {
          let editMessage = "";
          let deleteMessage = "";
  
          if (role == "admin") {
            editMessage = "Modifica";
            deleteMessage = "Elimina";
          }
          return (
            <div>
              <Popconfirm
                placement="topLeft"
                title={"Sei sicuro? "}
                onConfirm={() => deleteCoursesDetail(record["id"])}
                okText="Si"
                cancelText="No"
              >
                <a href="#">{deleteMessage}</a>
              </Popconfirm>
  
              <Popconfirm
                placement="topLeft"
                title={"Sei sicuro? "}
                onConfirm={() => editDetailCourses(record)}
                okText="Si"
                cancelText="No"
              >
                <a href="#"> {editMessage}</a>
              </Popconfirm>
            </div>
          );
        },
      },
    ];

    
  function RenderAdminSave(){
    if (role=="admin"){
    return (
    <div>

    <Button
      style={{ marginLeft: 20 }}
      type="primary"
      onClick={() => {
        setModalNew(true);
      }}
    >
      Aggiungi Link
    </Button>
    </div>
    )}
    else return <div></div>
}


const onFinishNew = (values) => {
    let bodyReq = JSON.stringify(values);
    setLoading(true)
    setModalNew(false)

    fetch(
        `${config.baseUrl}/courses/detail/id/${id_courses}`,
        getRequest(requestMethod.POST, bodyReq)
      )
        .then(handleResponse)
        .then((response) => {
            refreshDetail()
         
        })

  };
  const handleCancelNew = (values) => {
    setModalNew(false)
  };

  const handleCancelEdit = (values) => {
    setModalEdit(false)
  };


    

    return (
        <div
        className="site-layout-background"
        style={{ padding: 24, marginTop: 10 }}
      >
                       <PageHeader
            className="site-page-header"
            title={coursesLabel}
            subTitle="Elenco link"
  />
        <Table dataSource={dataSource} columns={columns} loading={loading} />
        <Modal
        title="Nuovo Link"
        visible={modalNew}
        destroyOnClose={true}
        closeIcon={[<div></div>]}
        footer={[
          <Button key="back" onClick={handleCancelNew}>
            Indietro
          </Button>,
        ]}
      >
        <Form name="basic" onFinish={onFinishNew}>
          <Form.Item
            label="Nome"
            name="label"
            layout="horizontal"
            rules={[{ required: true, message: "Inserisci nome" }]}
          >
            <Input placeholder={"Inserisci nome"} />
          </Form.Item>
          <Form.Item
            label="Link"
            name="url"
            layout="horizontal"
            rules={[{ required: true, message: "Inserisci Link" }]}
          >
            <Input placeholder={"Inserisci link"} />
          </Form.Item>
          <Form.Item
            label="Colore"
            name="color"
            rules={[{ required: true, message: "Inserisci colore" }]}
          >
            <Select placeholder={"Inserisci colore"}>
              <Select.Option value="#f44336">Rosso</Select.Option>
              <Select.Option value="#f06292">Rosa</Select.Option>
              <Select.Option value="#9c27b0">Viola</Select.Option>
              <Select.Option value="#2196f3">Blue</Select.Option>
              <Select.Option value="#00bcd4">Ciano</Select.Option>
              <Select.Option value="#009688">Verde</Select.Option>
              <Select.Option value="#f57c00">Arancione</Select.Option>

            </Select>
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              Salva
            </Button>
          </Form.Item>
        </Form>
      </Modal>
      <Modal
      visible={modalEdit}
      destroyOnClose={true}
      closeIcon={[<div></div>]}
      footer={[
        <Button key="back" onClick={handleCancelEdit}>
          Indietro
        </Button>
        
        ]}
      >
        <Form name="basic" onFinish={onFinishEdit}>
          <Form.Item
            label="Nome Link"
            name="label"
            layout="horizontal"
            rules={[{ required: true, message: "Inserisci nome corso" }]}
          >
            <Input placeholder={tmpLabel} />
          </Form.Item>
          <Form.Item
            label="Link"
            name="url"
            layout="horizontal"
            rules={[{ required: true, message: "Inserisci Link" }]}
          >
            <Input placeholder={tmpUrl} />
          </Form.Item>
          <Form.Item
            label="Colore"
            name="color"
            rules={[{ required: true, message: "Inserisci colore" }]}
          >
            <Select placeholder={"Seleziona colore"} onChange={(e)=>setTmpColor(e)}>
              <Select.Option value="#f44336">Rosso</Select.Option>
              <Select.Option value="#f06292">Rosa</Select.Option>
              <Select.Option value="#9c27b0">Viola</Select.Option>
              <Select.Option value="#2196f3">Blue</Select.Option>
              <Select.Option value="#00bcd4">Ciano</Select.Option>
              <Select.Option value="#009688">Verde</Select.Option>
              <Select.Option value="#f57c00">Arancione</Select.Option>

            </Select>
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Modifica
            </Button>
          </Form.Item>
        </Form>
        </Modal>
      <RenderAdminSave/>

        </div>
    )
}

export default DetailCourse;