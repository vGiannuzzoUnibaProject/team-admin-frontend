import React, { useState, useEffect } from "react";
import "antd/dist/antd.css";
import "./LoginBackendComponent.css";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import { Alert, Form, Input, Button, Row, Col, Typography, Image } from "antd";
import { config } from "../helpers/baseUrl";
import { requestMethod } from "../helpers/requestMethod";

import { getRequest } from "../helpers/getRequest";
import handleResponse from "../services/user";

import { useLocation } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import { userActions } from "../redux/actions/user";
import logo from "../static/logo.png";

function ChangePassword() {
 
  const [loading, setLoading] = useState(false);
  const [errMess, setErrMess] = useState();
  const [okMess, setOkMess] = useState();

  const onFinishFailed = (errorInfo) => {


};

const onFinish = (values) => {

    if(values.oldPassword != values.oldPassword2){
        setErrMess("Le due password devono essere uguali")
    }else {
        const bodyReq={"oldPassword":values.oldPassword, "newPassword": values.newPassword }
        fetch(`${config.baseUrl}/user/changepassword`, getRequest(requestMethod.PUT,JSON.stringify(bodyReq)))
        .then(handleResponse)
        .then((response) => {
            setOkMess("Password cambiata correttamente");
            setErrMess("")
        })
        .catch((error) => {
            console.log(error)
            setErrMess("Errore")
        });
    }


    }
    

  // reset login status

  const { Title } = Typography;

  const layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 8,
    },
  };
  const tailLayout = {
    wrapperCol: {
      offset: 8,
      span: 16,
    },
  };
  return (
    <div
    className="site-layout-background"
    style={{ padding: 24, marginTop: 10 }}
  >
      <Form
      {...layout}
      name="basic"
      onFinish={onFinish}>
    <Form.Item
        label="Vecchia Password"
        name="oldPassword"
        rules={[{ required: true, message: 'Inserisci vecchia password' }]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item
        label="Ripeti Vecchia Password"
        name="oldPassword2"
        rules={[{ required: true, message: 'Inserisci vecchia password' }]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item
        label="Nuova Password"
        name="newPassword"
        rules={[{ required: true, message: 'Inserisci nuova password' }]}
      >
        <Input.Password />
      </Form.Item>
          
      <Form.Item {...tailLayout}>
        <Button type="primary" htmlType="submit">
          Cambia password
        </Button>
      </Form.Item>
      </Form>
      
      {errMess ? (
      <div style={{width: "45%",alignContent:'right',marginLeft: "25%"}}>
      <Alert
              message={errMess}
              banner
              type="error"
             /></div>) : ""}
    {okMess ? (
      <div style={{width: "45%",alignContent:'right',marginLeft: "25%"}}>
      <Alert
              message={okMess}
              banner
              type="success"
             /></div>) : ""}


      </div>    
  );
}

export default ChangePassword;
