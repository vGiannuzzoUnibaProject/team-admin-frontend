
import React, { useState, useEffect } from "react";
import { config } from "../helpers/baseUrl";
import { requestMethod } from "../helpers/requestMethod";

import { getRequest } from "../helpers/getRequest";
import handleResponse from "../services/user";
import AccessDashboard from "./dashboards/AccessDashboard"
import ActionsDashboard from "./dashboards/ActionsDashboard"

function Dashboard(props){

    const [numberAccess,setNumberAccess]=useState([])
    const [numberActions,setNumberActions]=useState([])

    useEffect(() => {
        async function load() {
          fetch(`${config.baseUrl}/dashboard/access`, getRequest(requestMethod.GET))
            .then(handleResponse)
            .then((response) => {
            setNumberAccess(response.result);
            })
            .catch((error) => {
                console.log(error)
            })
        }
        
        async function load_actions() {
             fetch(`${config.baseUrl}/dashboard/actions`, getRequest(requestMethod.GET))
            .then(handleResponse)
            .then((response) => {
            setNumberActions(response.result);
            })
            .catch((error) => {
                console.log(error)
            })
        }
        
        
        load();
       load_actions();
      }, []);

    return (
<div style={{ padding: 24, marginTop: 10 }}
  > 
    <div className="site-layout-background" style={{ width: "40%",display: "inline-block" }}>

      <AccessDashboard data={numberAccess} x={'access_time'} y={"count"}/>
      </div>
     < div  className="site-layout-background" style={{ width: "50%", heigth: 300,marginLeft: 50, display: "inline-block" }}>

      <ActionsDashboard data={numberActions} angle={'number'} color={"action"}/>
      </div>
        </div>
    )
}
export default Dashboard