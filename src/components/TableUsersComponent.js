import React, { useState, useEffect } from "react";
import handleResponse from "../services/user";
import { config } from "../helpers/baseUrl";
import {
  Table,
  Tag,
  Modal,
  Switch,
  Button,
  Form,
  Input,
  Select,
  Alert,
  AutoComplete,
  Popconfirm,
  message,
  Result
} from "antd";
 

import { DeleteTwoTone,SmileOutlined } from "@ant-design/icons";
import { requestMethod } from "../helpers/requestMethod";

import { getRequest } from "../helpers/getRequest";

//https://it.reactjs.org/docs/animation.html

import "./TableLogComponent.css";
import { changeConfirmLocale } from "antd/lib/modal/locale";

function TableUsers(props) {
  const [loading, setLoading] = useState(true);
  const [listUser, setListUser] = useState([]);
  const [modalNewUserVisible, setModalNewUserVisible] = useState(false);
  const [loadingInsert, setLoadingInsert] = useState(false);
  const [disabledInsert, setdisabledInsert] = useState(false);
  const [tmpOkStatus, setOkStatus] = useState(false);
  const [modalResetVisible, setModalResetVisible] = useState(false);
  const [messageReset,setMessageReset]= useState('')
  const [tmpErrorStatus, setTmpErrorStatus] = useState(false);
  const [tmpPassword, setTmpPassword] = useState();
  const [okMessage, setOkMessage] = useState("");
  const errorMessage =
    "Errore nel salvataggio, username esistente o parametri mancanti. Contattare l'amministratore";

  useEffect(() => {
    async function load() {
      fetch(`${config.baseUrl}/all_users/`, getRequest(requestMethod.GET))
        .then(handleResponse)
        .then((response) => {
          setListUser(response.result);
          setLoading(false);
        })
        .catch((error) => {
          console.log(error);
        });
    }

    load();
  }, []);

  const prefixMessageOk = "Salvataggio effettuato con successo, con password: ";
  const onFinish = (values) => {
    console.log("Success:", values);
    let passwordTmp = Math.random().toString(36).slice(-8);
    values["password"] = passwordTmp;
    let bodyReq = JSON.stringify(values);
    setdisabledInsert(true);
    setLoadingInsert(true);
    fetch(
      `${config.baseUrl}/register`,
      getRequest(requestMethod.POST, bodyReq)
    )
      .then(handleResponse)
      .then((response) => {
        setLoadingInsert(false);
        setOkMessage(prefixMessageOk + passwordTmp);
        console.log(okMessage);
        setOkStatus(true);
        console.log(response);
        refreshUsers();
      })
      .catch((error) => {
        console.log("errore");
        setTmpErrorStatus(true);
        setModalNewUserVisible(true);
        refreshUsers();
      });
  };

  const handleCancel = () => {
    setModalNewUserVisible(false);
  };

  function handleSwitchChange(e, object) {
    setListUser(
      listUser.map((x) => {
        console.log(x);

        if (x.id !== object.id) return x;
        return { ...x, enabled: !e };
      })
    );
  }

  function refreshUsers() {
    setLoading(true);
    fetch(`${config.baseUrl}/all_users/`, getRequest(requestMethod.GET))
      .then(handleResponse)
      .then((response) => {
        setListUser(response.result);
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  function enableDisableUsers() {
    let bodyReq = {};
    const body_req = listUser.map((element) => {
        console.log(element)
      bodyReq[element.id] = element.enabled ? 1 : 0;
    });

    console.log(bodyReq)
    fetch(
      `${config.baseUrl}/user/enabledisable`,
      getRequest(requestMethod.PUT, JSON.stringify(bodyReq))
    )
      .then(handleResponse)
      .then(() => refreshUsers());
  }

  function changeRole(id, value) {
    let bodyReq = { id: id, value: value };
    fetch(
      `${config.baseUrl}/user/changerole`,
      getRequest(requestMethod.PUT, JSON.stringify(bodyReq))
    )
      .then(handleResponse)
      .then((response) => {
        refreshUsers();
      });
  }

  function deleteUser(id) {
    fetch(`${config.baseUrl}/user/id/${id}`, getRequest(requestMethod.DELETE))
      .then(handleResponse)
      .then((response) => {
        refreshUsers();
      });
  }

  function resetPassword(id,username) {
    let password = Math.random().toString(36).slice(-8);
    const body_req={"password": password}
    fetch(`${config.baseUrl}/user/resetpassword/id/${id}`, getRequest(requestMethod.PUT,JSON.stringify(body_req)))
    .then(handleResponse)
    .then((response) => {
        setModalResetVisible(true);
        setMessageReset("La password dell'utente " + " è stata resettata con: " + password)
    });

  }

  const dataSource = listUser;
  const columns = [
    {
      title: "Nome",
      dataIndex: "first_name",
    },
    {
      title: "Cognome",
      dataIndex: "last_name",
    },
    {
      title: "username",
      dataIndex: "username",
    },
    {
      title: "Ruolo",
      dataIndex: "role",
      render: (text) => {
        let color = null;
        if (text === "admin") {
          color = "geekblue";
        } else color = "green";

        return (
          <Tag color={color} key={text}>
            {text}
          </Tag>
        );
      },
    },
    {
      title: "Creato da",
      dataIndex: "created_by",
      render: (text) => {
        let created = null;
        if (!text) created = "admin";
        else created = text;

        return <div>{created}</div>;
      },
    },
    {
      title: "Abilitato",
      dataIndex: "enabled",
      render: (e, record) => {
        let disabled = false;
        if (record["username"] == "admin") disabled = true;
        return (
          <Switch
            checked={record["enabled"]}
            disabled={disabled}
            onChange={() => handleSwitchChange(e, record)}
          />
        );
      },
    },
    {
      title: "Azione",
      key: "action",
      render: (text, record) => {
        console.log("record");
        let role = record["role"];
        let messageAdmin = "";
        let change = "";
        let deleteMessage = "";
        let resetMessage = "";

        if (record["username"] == "admin") {
          messageAdmin = "";
          change = "";
          deleteMessage = "";
          resetMessage = "";
        }
        if (role == "admin" && record["username"] != "admin") {
          messageAdmin = "cambia in Viewer";
          change = "viewer";
          deleteMessage = "  Elimina utente";
          resetMessage = "   Reset password";
        }
        if (role == "viewer") {
          messageAdmin = "cambia in Admin";
          change = "admin";
          deleteMessage = "  Elimina utente";
          resetMessage = "   Reset password";
        }

        return (
          <div>
            <a href="#" onClick={() => changeRole(record["id"], change)}>
              {messageAdmin}
            </a>
            <Popconfirm
              placement="topLeft"
              title={"Sei sicuro? "}
              onConfirm={() => deleteUser(record["id"])}
              okText="Si"
              cancelText="No"
            >
              <a href="#">{deleteMessage}</a>
            </Popconfirm>
            <Popconfirm
              placement="topLeft"
              title={"Sei sicuro? "}
              onConfirm={() => resetPassword(record["id"],record['username'])}
              okText="Si"
              cancelText="No"
            >
              <a href="#">{resetMessage}</a>
            </Popconfirm>
          </div>
        );
      },
    },
  ];

  return (
    <div
      className="site-layout-background"
      style={{ padding: 24, marginTop: 10 }}
    >
      <Table dataSource={dataSource} columns={columns} loading={loading} />
      <Button
        type="primary"
        loading={loading}
        onClick={() => enableDisableUsers()}
      >
        Salva
      </Button>

      <Button
        style={{ marginLeft: 20 }}
        type="default"
        onClick={() => {
          setModalNewUserVisible(true);
          setOkStatus(false);

          setdisabledInsert(false);
          setLoadingInsert(false);
          setTmpErrorStatus(false);
        }}
      >
        Aggiungi Utente
      </Button>
      <Modal
        title="Nuovo utente"
        visible={modalNewUserVisible}
        destroyOnClose={true}
        closeIcon={[<div></div>]}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Indietro
          </Button>,
        ]}
      >
        <Form name="basic" onFinish={onFinish}>
          <Form.Item
            label="Nome"
            name="firstname"
            layout="horizontal"
            rules={[{ required: true, message: "Inserisci nome" }]}
          >
            <Input placeholder={"Inserisci nome"} />
          </Form.Item>

          <Form.Item
            label="Cognome"
            name="lastname"
            rules={[{ required: true, message: "Inserisci cognome" }]}
          >
            <Input placeholder={"Inserisci cognome"} />
          </Form.Item>

          <Form.Item
            label="Username"
            name="username"
            
            rules={[{ required: true, message: "Inserisci username" }]}
          >
            <Input maxLength={45} placeholder={"Inserisci username"} />
          </Form.Item>

          <Form.Item
            label="Ruolo"
            name="role"
            rules={[{ required: true, message: "Inserisci Ruolo" }]}
          >
            <Select placeholder={"Inserisci ruolo"}>
              <Select.Option value="viewer">Viewer</Select.Option>
              <Select.Option value="admin">Admin</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item>
            {tmpErrorStatus ? (
              <Alert message={errorMessage} type="error" />
            ) : (
              ""
            )}
            {tmpOkStatus ? <Alert message={okMessage} type="success" /> : ""}
            <Button
              type="primary"
              htmlType="submit"
              loading={loadingInsert}
              disabled={disabledInsert}
            >
              Salva
            </Button>
          </Form.Item>
        </Form>
      </Modal>
      <Modal
      visible={modalResetVisible}
      destroyOnClose={true}
      onCancel={()=>{setModalResetVisible(false); setMessageReset('')}}>
          <Result
    icon={<SmileOutlined />}
    title={messageReset}
  />,

      </Modal>
    </div>
  );
}

export default TableUsers;
