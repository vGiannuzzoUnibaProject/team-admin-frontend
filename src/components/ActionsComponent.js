import React, { useState, useEffect } from "react";
import handleResponse from "../services/user";
import { config } from "../helpers/baseUrl";
import {
  Table,
  Tag,
  Modal,
  Switch,
  Button,
  Form,
  Input,
  Select,
  Alert,
  AutoComplete,
  Popconfirm,
  message,
  Result
} from "antd";
 

import { DeleteTwoTone,SmileOutlined } from "@ant-design/icons";
import { requestMethod } from "../helpers/requestMethod";

import { getRequest } from "../helpers/getRequest";

//https://it.reactjs.org/docs/animation.html

import "./TableLogComponent.css";
import { changeConfirmLocale } from "antd/lib/modal/locale";

function Actions(props) {
  const [loading, setLoading] = useState(true);
  const [listActions, setListActions] = useState([]);
  
  

  useEffect(() => {
    async function load() {
      fetch(`${config.baseUrl}/listActions/`, getRequest(requestMethod.GET))
        .then(handleResponse)
        .then((response) => {
            console.log(response)
            setListActions(response.result);
          setLoading(false);
        })
        .catch((error) => {
          console.log(error);
        });
    }

    load();
  }, []);

  const dataSource = listActions;
  const columns = [
    {
        title: "Data",
        dataIndex: "data_access",
      },
      {
        title: "Ora",
        dataIndex: "hour_access",
      },
    {
      title: "Username",
      dataIndex: "username",
    },
    {
      title: "Numero linee",
      dataIndex: "number_rows",
    },
    {
      title: "Azione",
      dataIndex: "action",
      render: (text) => {
        console.log(text)
        let color = null;
        if (text === "delete") {
          color = "volcano";
        } else color = "green";

        return (
          <Tag color={color} key={text}>
            {text}
          </Tag>
        );
      },
    },
  ];

  return (
    <div
      className="site-layout-background"
      style={{ padding: 24, marginTop: 10 }}
    >
      <Table dataSource={dataSource} columns={columns} loading={loading} />
    </div>
  );
}

export default Actions;
