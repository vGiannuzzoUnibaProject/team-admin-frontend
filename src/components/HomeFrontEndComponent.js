import MicrosoftLogin from "react-microsoft-login";
import { logout } from "react-microsoft-login";

import React, { useState, useEffect } from "react";
import handleResponse from "../services/user";
import { getRequest } from "../helpers/getRequest";
import { requestMethod } from "../helpers/requestMethod";
import { tenantMicrosoft } from "../helpers/tenantMicrosoft"
import { config } from "../helpers/baseUrl";
import {
  Card,
  Form,
  Input,
  Button,
  Popconfirm,
  message,
  Spin,
  InputNumber,
  Typography,
  Modal,
  Result,
  Row,
  Col,
  Image,
} from "antd";
import "./HomeFrontEnd.css";
import logo from "../static/user.png";
import { Link } from "react-router-dom";

const { Title } = Typography;

const publicIp = require('public-ip');
function HomeFrontEnd() {
  

const [displayLogin, setDisplayLogin] = useState("");
  const [displayResultOk, setDisplayResultOk] = useState("none");
  const [displayResultError, setDisplayResultError] = useState("none");
  const [loading,setLoading]=useState(false)
  const [msalInstance, onMsalInstanceChange] = useState();
  const [resultSso, setResultSso] = useState([]);
  const [publicIpLoaded,setPublicIpLoaded]=useState("")
  const [courses, setCourses] = useState([]);

  const requestLog = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json'},
  }

  function refreshPage() {
    window.location.reload(false);
  }


  useEffect(() => {
    const requestIP = {
      method: 'GET',
      headers: { 'Content-Type': 'application/json'},
    }
    async function load_ip() {
      await (
        publicIp.v4()
      )
      .then((response) => {
       setPublicIpLoaded(response)  

      })
        .catch((error) => {
          console.log("error" + error);
        });
    }
    load_ip();

  }, []);


  const authHandler = (err, data, msal) => {
    if (data) {
      onMsalInstanceChange(msal)
      delete data.idToken;
      delete data.idTokenClaims;
      delete data.account
      data['ip_login']=publicIpLoaded
      setResultSso({...data});
      requestLog['body']=JSON.stringify(data)
      setLoading(true)
      fetch(
        `${config.baseUrl}/register/access`,requestLog
      )
        .then(response => {
          if(response.ok){
          //  

          }else       throw new Error('Something went wrong.');

        })
        .catch((error) => {
          console.log("error" + error);
          setDisplayResultOk("none");   
          setDisplayResultError(""); 
        });

        fetch(`${config.baseUrl}/courses/enabled`, getRequest(requestMethod.GET))
        .then((response) => {
         response.text().then(function (text) {
            const data = text && JSON.parse(text);
            setDisplayLogin("none");
            setDisplayResultOk("");   
            setDisplayResultError("none"); 
            setCourses(data.result)
            setLoading(false)
          });
        
        })
        .catch((error) => {
          console.log(error);
        });
         
    }
  };


  const logoutHandler = () => {
    msalInstance.logout();
  };


  function RenderCourses(props){
    if(!props.data)
      return <div></div>

    return props.data.map((item)=>{
     console.log(item)
      return(
        <Col span={8}>
          <a target="_blank" href={item.url}>
        <Card title={item.label} 
        bordered={true}
        hoverable={true}
        style={{marginBottom: 30}}

       >
          {
            item.detail.map((detail)=>{
              return (
                <Button style={{backgroundColor: detail.color, color:"white", marginRight: 10,marginBottom: 10}}><a target="_blank" href={detail.url}>{detail.label}</a></Button>
              )
            })
            
          }
        </Card>
        </a>
      </Col>
      )
    })

    
    
  }

  return (
    <Row
      type="flex"
      className="background"
      justify="center"
      style={{ minHeight: "100vh", paddingTop: 80 }}
    >
      <Col span={12}>
        <div style={{ display: displayLogin }}>
          <div type="flex" justify="center" align="middle">
            <Image width={150} height={150} src={logo} />
            <Title style={{ color: "white" }}>Registra Accesso</Title>
          </div>

          <div id={"buttonLoginMicrosoft"}>
            <MicrosoftLogin
              clientId={tenantMicrosoft.clientId}
              withUserData={tenantMicrosoft.withUserData}
              authCallback={authHandler}
              redirectUri={tenantMicrosoft.redirectUri}
              tenantUrl={
                tenantMicrosoft.tenantUrl
              }
              debug={tenantMicrosoft.debug}
            />
          </div>
          
        </div>

        <div style={{ display: displayResultOk }}>
          
          <Result
            status="success"
            title={<Title style={{ color: "white" }}>Accesso riuscito</Title>}
            extra={
            <div>
              <Button onClick={refreshPage} type="primary" key="console">
            Aggiorna la pagina
          </Button>
          <Button type={"danger"} onClick={logoutHandler}>Logout</Button>

          </div>}
          />
           <div>
           <Title  style={{color:"white",textAlign: "center"}} level={2}>Corsi attivi</Title>

          <Row gutter={16}>

          <RenderCourses data={courses}/>
          </Row>

          </div>

          

        </div>
        <div style={{ display: displayResultError }}>
          <Result
            status="warning"
            title={<Title style={{ color: "white" }}>Errore riprovare</Title>}
            extra={<Button onClick={refreshPage} type="primary" key="console">
            Aggiorna la pagina
          </Button>}
            
          />
          
        </div>
      </Col>
    </Row>
  );
}

export default HomeFrontEnd;
