import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import MainLayout from './MainLayoutComponent'
import { useDispatch, useSelector } from 'react-redux';

function PrivateRoute({roles,View, ...rest}) {
    const loggedIn = useSelector(state => state.authentication.loggedIn);
    return (
        <Route {...rest} render={props => {
            if (!localStorage.getItem('user') || !loggedIn) {
                
                // not logged in so redirect to login page with the return url
                return <Redirect to={{ pathname: '/admin/login', state: { from: props.location } }} />
            }

            // logged in so return component
            props.view=View;
            
            return <MainLayout {...props} />
        }} />
    );
}

export default PrivateRoute;