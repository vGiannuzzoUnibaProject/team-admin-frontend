import React, { useState, useEffect } from "react";
import './FilterComponent.css'
import { Table, Input,Tag, Space, Switch, Button, Form, Alert,DatePicker,AutoComplete } from "antd";
import moment from 'moment';
import {
    ArrowDownOutlined ,
    DownSquareTwoTone,
    DeleteTwoTone,
      MoreOutlined,
      EditOutlined,
      PlayCircleOutlined,
      BorderOutlined,
      SearchOutlined,
      SaveOutlined,
      StepBackwardOutlined,
      RollbackOutlined,
      SyncOutlined,
    } from "@ant-design/icons";

const layout = {
    labelCol: { span: 11 },
    wrapperCol: { span: 3 },
    formLayout: 'inline'

  };
 
  const tailLayout = {
    wrapperCol: {
      span: 24,
    }}

    const dateFormat = 'DD/MM/YYYY';

function Filter(props){
const today = new Date();
  const dd = String(today.getDate()).padStart(2, '0');
  const mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  const yyyy = today.getFullYear()
  
    return (<div>
<div className="buttonCode" style={{textAlign: "center",backgroundColor: "white",paddingTop:10,paddingBottom:1}}>
    
    <Form  {...layout} 
    onFinish={props.onFinish}
    layout= "inline"
    style={{marginBottom:10,marginLeft:10,textAlign:"center"}}
    >
    <Form.Item name="from" label="Dal">
         
          <DatePicker
          style={{width:150}}
          format={dateFormat}
          placeholder={"Seleziona data"}
          />
    </Form.Item>
    <Form.Item name="to" label="Al">
          <DatePicker
          style={{width:150}}
          format={dateFormat}
          placeholder={"Seleziona data"}
          />
    </Form.Item>
    <Form.Item name="username"label="Username">
    <AutoComplete
              style={{width:300}}

    dataSource={props.listUser}
       options={"username",props.listUser}
        placeholder="Inserisci Username"
      />
    </Form.Item>
    <Form.Item {...tailLayout}>
      
    <Button
      icon={<SearchOutlined />}
      style={{ marginRight: 5 }}
      type="primary" htmlType="submit"
      >

          
      
    
    
      Ricerca
    </Button>
    </Form.Item>
    </Form>
    <div className="errorMessage">
        {props.message ? <Alert message={props.message} type="error" /> : ""}
        </div>
    </div>
    </div>
    
    )

}

export default Filter