import React, { useState, useEffect } from "react";
import handleResponse from "../services/user";
import { config } from "../helpers/baseUrl";
import { Table, Tag, Space, Switch, Button, Form, Alert,DatePicker,AutoComplete,
  Popconfirm,message } from "antd";
import { requestMethod } from "../helpers/requestMethod";
import { today } from "../helpers/today";

import { getRequest } from "../helpers/getRequest";
import moment from 'moment';
import Filter from './FilterComponent'
import { exportCSV} from "../helpers/exportCsv"
import {
ArrowDownOutlined ,
DownSquareTwoTone,
DeleteTwoTone,
  MoreOutlined,
  EditOutlined,
  PlayCircleOutlined,
  BorderOutlined,
  SearchOutlined,
  SaveOutlined,
  StepBackwardOutlined,
  RollbackOutlined,
  SyncOutlined,
} from "@ant-design/icons";
//https://it.reactjs.org/docs/animation.html
import { CSVLink, CSVDownload } from "react-csv";

import './TableLogComponent.css'


function TableLog() {
  
  const [log, setLog] = useState([]);
  const [loading, setLoading] = useState(true);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [selectedRowValue, setSelectedRowValue] = useState([]);
  const [fromPicker, setFromPicker] = useState("");
  const [toPicker, setToPicker] = useState("");
  const [listUser, setListUser] = useState([]);
  const [searchedResult, setSearchedResult] = useState(false);
  const [noRequest,setNoRequest]=useState("")
  const limit=250
 
  const text = 'Sei sicuro di voler elimnomare i log?';
  const limitLog = "Visualizzati i primi "+ limit +" log"
  const dateFormat = 'DD/MM/YYYY';

  useEffect(() => {
    async function load() {
      fetch(`${config.baseUrl}/logs/limit/${limit}`, getRequest(requestMethod.GET))
        .then(handleResponse)
        .then((response) => {
          console.log(response)
          setLog(response.result);
          setLoading(false);
        })
        .catch((error) => {
            console.log(error)
        })
    }
    async function load_user() {
        fetch(`${config.baseUrl}/all_username_log/`, getRequest(requestMethod.GET))
          .then(handleResponse)
          .then((response) => {
            setListUser(response.result);
          })
          .catch((error) => {
              console.log(error)
          })
    }
    
    load();
    load_user();
  }, []);
  
  function italianToStandard(date,format){
    console.log(date)

    date=date.split(format)
    console.log(date)

    return date[2]+'-'+date[1]+'-'+date[0]
  }

  function dateToTimestamp(date,time){
    date=italianToStandard(date,'-')
    let timestamp = new Date(date+ " "+time);
    timestamp = timestamp.getTime();

    return timestamp/1000

  }

  function refreshLoading(){
    fetch(`${config.baseUrl}/logs/limit/${limit}`, getRequest(requestMethod.GET))
    .then(handleResponse)
    .then((response) => {
      setLog(response.result);
      setLoading(false);
    })
    .catch((error) => {
        console.log(error)
    })
}
  
  function confirmDelete() {
    let timestamp=[]
    let body=[]
    selectedRowValue.forEach(value => {
      timestamp.push({"id":value.id, "date":italianToStandard(value.data_access,'-'),"time": value.hour_access})
      
    });
    setLoading(true);
    fetch(`${config.baseUrl}/logs/delete`, getRequest(requestMethod.DELETE,JSON.stringify(timestamp)))
    .then(handleResponse)
    .then((response) => {
      console.log(timestamp)
      refreshLoading()
    })
    .catch((error) => {
        console.log(error)
    })
    

    const bodyReq={"type": "delete", "number": timestamp.length}


    fetch(`${config.baseUrl}/actionLog`, getRequest(requestMethod.POST,JSON.stringify(bodyReq)))
    .then(handleResponse)
    .then((response) => {
     
    })
    .catch((error) => {
        console.log(error)
    })




   

  }
  const onSave = (values) => {

  let queryString=""
  if(values.username)  {
    values.username=values.username.replaceAll('.','$')

  queryString+="/mail/"+values.username
  }
  if(values.from)
  queryString+="/from/"+values.from._d.getTime()
  if(values.to)
  queryString+="/to/"+values.to._d.getTime()
    console.log(queryString)
    if(queryString){
      setSelectedRowKeys([])
      setLoading(true)
      setSearchedResult(true)
      setNoRequest("")
    fetch(`${config.baseUrl}/logs/search${queryString}`, getRequest(requestMethod.GET))
    .then(handleResponse)
    .then((response) => {
      console.log(response)
      setLog(response.result);
      setLoading(false);
    })
    .catch((error) => {
        console.log(error)
    })
  }else setNoRequest("Seleziona almeno un filtro")
}
async function load_user() {
    fetch(`${config.baseUrl}/logs`, getRequest(requestMethod.GET))
      .then(handleResponse)
      .then((response) => {
        setListUser(response.result);
      })
      .catch((error) => {
          console.log(error)
      })


  }

  const dataSource = log;
  const columns = [
    {
      title: "Data",
      dataIndex: "data_access",
    },
    {
      title: "Ora",
      dataIndex: "hour_access",
    },
    {
      title: "Nome e Cognome",
      dataIndex: "displayName",
     
    },
    {
      title: "Mail",
      dataIndex: "mail",
     
    },
    {title: "IP",
    dataIndex: "public_ip",
   
  },
  ];

  function saveActionLog(type,numberRows){
    const bodyReq={"type": type, "number": numberRows}
    fetch(`${config.baseUrl}/actionLog`, getRequest(requestMethod.POST,JSON.stringify(bodyReq)))
      .then(handleResponse)
      .then((response) => {
      })
      .catch((error) => {
          message.error("Errore salvataggio log")
      })

  }
  const exportSelected= (values) =>{
    let data=[]
    console.log(values)
    saveActionLog('export',values.length)
    for (var i = 0; i < values.length; i++) {
      console.log(values[i])
      data.push({"data_access": values[i].data_access, 
      "hour_access": values[i].hour_access, "mail": values[i].mail, 
      "public_ip": values[i].public_ip,
    "displayName": values[i].displayName })
    }
   

    let namefile="Export_"+today()+'.csv'

    exportCSV(data,namefile)
  }

  const rowSelection = {
    selectedRowKeys: selectedRowKeys,
    onChange: (selectedRowKeys, selectedRows) => {
      setSelectedRowKeys(selectedRowKeys)
      
      setSelectedRowValue(selectedRows.filter(item => item !== selectedRowKeys)
      )
    }

  };

  return (
    <div>
   <Filter from={fromPicker} to={toPicker} listUser={listUser} message={noRequest}
   onFinish={(values) => {
   
     onSave(values)
  }}></Filter>

    
    <div>
    <Button
      icon={<DownSquareTwoTone twoToneColor="FFFFFF"/>}
      onClick={()=>exportSelected(selectedRowValue)}
      disabled={!selectedRowKeys.length}
      style={{borderColor: "green"}}
      className="lineBottom"
    >
      Export CSV
    </Button>
    <Popconfirm
     title={text}
     onConfirm={confirmDelete}
     okText="Si"
     cancelText="No"
     disabled={!selectedRowKeys.length}
     >
       
    <Button
      icon={<DeleteTwoTone  twoToneColor="#eb2f96"/>}
      danger
      disabled={!selectedRowKeys.length}
    >
      Elimina Log
    </Button>
    </Popconfirm>

    </div>

    
    <div className="site-layout-background" style={{ padding: 24,marginTop:10 }}>
     { searchedResult ? "" : limitLog}
        <Table dataSource={log} 
        rowSelection={rowSelection}
        pagination={{ pageSize: 9999999999999999999 }}
        columns={columns}
         loading={loading} />
       
    </div>
    </div>
  );
}

export default TableLog;
