import React, { useEffect } from "react";
import { Button, Row, Col, Typography, Result } from "antd";
import "antd/dist/antd.css";
import { Link, useLocation } from "react-router-dom";

const { Title } = Typography;

function Error(props) {
  const location = useLocation();
  const ErrorCode = location.state;

  let link = "/";
  let linkTitle = "Back home";

  let title = "Sorry some error occurred: error " + ErrorCode;
  let subtitle = "Please contact support service";
  let status = "warning";

  return (
    <Row
      type="flex"
      className="background"
      justify="center"
      align="middle"
      style={{ minHeight: "100vh" }}
    >
      <Col span={12}>
        <div
          type="flex"
          justify="center"
          align="middle"
          style={{ marginBottom: "40px" }}
        >
          <Result
            status={status}
            title={title}
            subTitle={[<div style={{ fontSize: "2em" }}>{subtitle}</div>]}
            extra={
              <Link to={link}>
                <Button type="primary">{linkTitle}</Button>
              </Link>
            }
          />
        </div>
      </Col>
    </Row>
  );
}

export default Error;
