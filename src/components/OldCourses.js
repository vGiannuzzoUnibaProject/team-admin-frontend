import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import handleResponse from "../services/user";
import { config } from "../helpers/baseUrl";
import {
  Table,
  Tag,
  Modal,
  Switch,
  Button,
  Form,
  Input,
  Select,
  Alert,
  AutoComplete,
  Popconfirm,
  message,
  Result,
} from "antd";

import { DeleteTwoTone, SmileOutlined } from "@ant-design/icons";
import { requestMethod } from "../helpers/requestMethod";

import { getRequest } from "../helpers/getRequest";

//https://it.reactjs.org/docs/animation.html

function Courses(props) {
  const role = useSelector((state) => state.users.role);
  const [loading, setLoading] = useState(true);
  const [courses, setCourses] = useState([]);
  const [modalNew, setModalNew] = useState(false);
  const [modalEdit, setModalEdit] = useState(false);
  const [tmpId,setTmpId]=useState()
  const [tmpLabel,setTmpLabel]=useState()
  const [tmpUrl,setTmpUrl]=useState()
  useEffect(() => {
    async function load() {
      fetch(`${config.baseUrl}/courses/all/`, getRequest(requestMethod.GET))
        .then(handleResponse)
        .then((response) => {
          setCourses(response.result);
          console.log(response);
          setLoading(false);
        })
        .catch((error) => {
          console.log(error);
        });
    }

    load();
  }, []);

  const onFinishEdit = (values) =>{
    console.log(values)
    setModalEdit(false)
    fetch(
        `${config.baseUrl}/courses/id/${tmpId}`,
        getRequest(requestMethod.PUT, JSON.stringify(values))
      )
        .then(handleResponse)
        .then(() => refreshCourses());
    }

  

  const onFinishNew = (values) => {
    let bodyReq = JSON.stringify(values);
    setLoading(true)
    setModalNew(false)

    fetch(
        `${config.baseUrl}/courses/new`,
        getRequest(requestMethod.POST, bodyReq)
      )
        .then(handleResponse)
        .then((response) => {
            refreshCourses()
         
        })

  };
  const handleCancelNew = (values) => {
    setModalNew(false)
  };

  const handleCancelEdit = (values) => {
    setModalEdit(false)
  };
  function refreshCourses() {
    setLoading(true);
    fetch(`${config.baseUrl}/courses/all/`, getRequest(requestMethod.GET))
      .then(handleResponse)
      .then((response) => {
        setCourses(response.result);
        console.log(response);
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  function deleteCourses(id) {
    fetch(
      `${config.baseUrl}/courses/id/${id}`,
      getRequest(requestMethod.DELETE)
    )
      .then(handleResponse)
      .then((response) => {
        refreshCourses();
      });
  }

  function enableDisableCourses() {
    let bodyReq = {};
    const body_req = courses.map((element) => {
      console.log(element);
      bodyReq[element.id] = element.enabled ? 1 : 0;
    });

    console.log(bodyReq);
    fetch(
      `${config.baseUrl}/courses/enable_disable`,
      getRequest(requestMethod.PUT, JSON.stringify(bodyReq))
    )
      .then(handleResponse)
      .then(() => refreshCourses());
  }

  function editCourses(object){
      setTmpId( object.id)
      setTmpLabel(object.label)
      setTmpUrl(object.url) 
      setModalEdit(true)


  }
  function handleSwitchChange(e, object) {
    setCourses(
      courses.map((x) => {
        if (x.id !== object.id) return x;
        return { ...x, enabled: !e };
      })
    );
  }

  //se role è diverso admin nascondi bottoni
  const dataSource = courses;

  const columns = [
    {
      title: "Nome Corso",
      dataIndex: "label",
    },
    {
      title: "Url",
      dataIndex: "url",
      
    },
    {
      title: "Abilitato",
      dataIndex: "enabled",
      render: (e, record) => {
        let disabled = false;
        if (role != "admin") disabled = true;
        return (
          <Switch
            checked={record["enabled"]}
            disabled={disabled}
            onChange={() => handleSwitchChange(e, record)}
          />
        );
      },
    },
    {
      title: "Azione",
      key: "action",
      render: (text, record) => {
        let editMessage = "";
        let deleteMessage = "";

        if (role == "admin") {
          editMessage = "Modifica corso";
          deleteMessage = "Elimina corso";
        }
        return (
          <div>
            <Popconfirm
              placement="topLeft"
              title={"Sei sicuro? "}
              onConfirm={() => deleteCourses(record["id"])}
              okText="Si"
              cancelText="No"
            >
              <a href="#">{deleteMessage}</a>
            </Popconfirm>

            <Popconfirm
              placement="topLeft"
              title={"Sei sicuro? "}
              onConfirm={() => editCourses(record)}
              okText="Si"
              cancelText="No"
            >
              <a href="#"> {editMessage}</a>
            </Popconfirm>
          </div>
        );
      },
    },
  ];

  function renderAdminSave(){
      if (role=="admin"){
      return (
          <div>
        <Button
        type="primary"
        loading={loading}
        onClick={() => enableDisableCourses()}
      >
        Salva
      </Button>

      <Button
        style={{ marginLeft: 20 }}
        type="default"
        onClick={() => {
          setModalNew(true);
        }}
      >
        Aggiungi Corso
      </Button>
      </div>
      )}
      else return <div></div>
  }

  return (
    <div
      className="site-layout-background"
      style={{ padding: 24, marginTop: 10 }}
    >
      <Table dataSource={dataSource} columns={columns} loading={loading} />

      
      <Modal
        title="Nuovo corso"
        visible={modalNew}
        destroyOnClose={true}
        closeIcon={[<div></div>]}
        footer={[
          <Button key="back" onClick={handleCancelNew}>
            Indietro
          </Button>,
        ]}
      >
        <Form name="basic" onFinish={onFinishNew}>
          <Form.Item
            label="Nome corso"
            name="label"
            layout="horizontal"
            rules={[{ required: true, message: "Inserisci nome corso" }]}
          >
            <Input placeholder={"Inserisci nome corso"} />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              Salva
            </Button>
          </Form.Item>
        </Form>
      </Modal>
      <Modal
      visible={modalEdit}
      destroyOnClose={true}
      closeIcon={[<div></div>]}
      footer={[
        <Button key="back" onClick={handleCancelEdit}>
          Indietro
        </Button>
        
        ]}
      >
        <Form name="basic" onFinish={onFinishEdit}>
          <Form.Item
            label="Nome corso"
            name="label"
            layout="horizontal"
            rules={[{ required: true, message: "Inserisci nome corso" }]}
          >
            <Input placeholder={tmpLabel} />
          </Form.Item>

          <Form.Item
            label="Indirizzo url completo"
            name="url"
            rules={[{ required: true, message: "Inserisci link corso url" }]}
          >
            <Input placeholder={tmpUrl}/>
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              Modifica
            </Button>
          </Form.Item>
        </Form>
        </Modal>

    </div>
  );
}

export default Courses;
