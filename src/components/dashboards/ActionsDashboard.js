import React, { useState, useEffect } from 'react';
import { Pie } from '@ant-design/charts';
import { Spin,Typography } from "antd";
import "./dashboard.css"

const title="Azioni Utenti"
const { Title } = Typography;
function ActionsDashboard(props) {
  const data = props.data
  if (data && data.length >0){
      console.log(data)
    const config = {
        data: data,
        isStack: true,
        heigth: 400,
        angleField: props.angle,
        colorField: props.color,
        color: ({ action }) => {
            console.log(action)
            if(action == 'export'){
              return '#62DAAB';
            }
            return '#E25141';
        },
        animation: true,
        radius: 1,
        innerRadius: 0.6,
        label: {
          type: 'inner',
          offset: '-50%',
          content: '{value}',
          style: {
            textAlign: 'center',
            fontSize: 14,
          },
        },
        interactions: [{ type: 'element-selected' }, { type: 'element-active' }],
        statistic: {
          title: false,
          content: {
            style: {
              whiteSpace: 'pre-wrap',
              overflow: 'hidden',
              textOverflow: 'ellipsis',
            },
            formatter: function formatter() {
              return 'Azioni Utente';
            }
        }}
       
        

        

      };
      return (
          <div className="box_empty" >
            <Title level={4}>{title}</Title>
              <Pie {...config}/>
          </div>
      )
}
   return (
        <div style={{padding:24}} className="box_empty">
            <Title level={4}>{title}</Title>
            <div className="position_loading">
            <Spin size="large" />
            </div>
  </div>
    )
};
     

export default ActionsDashboard