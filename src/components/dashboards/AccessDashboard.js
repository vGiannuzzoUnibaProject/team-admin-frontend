import React, { useState, useEffect } from 'react';
import { Column } from '@ant-design/charts';
import { Spin,Typography } from "antd";
import "./dashboard.css"

const title="Numero accessi 30 giorni"
const { Title } = Typography;
function AccessDashboard(props) {
  const data = props.data
  if (data && data.length >0){
    const config = {
        data: data,
        isStack: true,
        xField: props.x,
        yField: props.y,
        animation: true,

        

      };
      return (
          <div className="box_empty" >
            <Title level={4}>{title}</Title>
              <Column {...config}/>
          </div>
      )
}
   return (
        <div style={{padding:24}} className="box_empty">
            <Title level={4}>{title}</Title>
            <div className="position_loading">
            <Spin size="large" />
            </div>
  </div>
    )
};
     

export default AccessDashboard