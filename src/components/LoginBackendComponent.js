import React, { useState, useEffect } from "react";
import "antd/dist/antd.css";
import "./LoginBackendComponent.css";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import { Alert, Form, Input, Button, Row, Col, Typography, Image } from "antd";

import { useLocation } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import { userActions } from "../redux/actions/user";
import logo from "../static/logo.png";

function Login() {
  const [submitted, setSubmitted] = useState(false);
  const loggingIn = useSelector((state) => state.authentication.loggingIn);
  const errMess = useSelector((state) => state.authentication.errMess);
  const dispatch = useDispatch();
  const location = useLocation();

  const [loading, setLoading] = useState(false);

  // reset login status
  useEffect(() => {
    dispatch(userActions.logout());
  }, []);

  useEffect(() => {
    if (errMess) {
      console.log("fine caricamento");
      setLoading(false);
    }
  }, [errMess]);

  const onFinish = (values) => {
    setLoading(true);
    let username = values.username;
    let password = values.password;
    setSubmitted(true);
    if (username && password) {
      // get return url from location state or default to home page
      const { from } = location.state || { from: { pathname: "/" } };
      dispatch(userActions.login(values.username, values.password, from));
    }
  };

  const onFinishFailed = (errorInfo) => {
  };

  const { Title } = Typography;

  const layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 8,
    },
  };
  const tailLayout = {
    wrapperCol: {
      offset: 8,
      span: 16,
    },
  };
  return (
    <Row
      type="flex"
      id="background-login-backend"
      justify="center"
      align="middle"
      style={{ minHeight: "100vh" }}
    >
      <Col span={12}>
        <div
          type="flex"
          justify="center"
          align="middle"
          style={{ marginBottom: "40px" }}
        >
          <Image width={220} height={220} src={logo} />
          <Title>Teams Administrator</Title>
        </div>
        <Form
          {...layout}
          name="basic"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label="Username"
            name="username"
            rules={[{ required: true, message: "Inserisci Username" }]}
          >
            <Input placeholder="Inserisci username" prefix={<UserOutlined />} />
          </Form.Item>
          <Form.Item
            label="Password"
            name="password"
            rules={[{ required: true, message: "Inserisci Password" }]}
          >
            <Input.Password
              placeholder="Inserisci password"
              prefix={<LockOutlined />}
            />
          </Form.Item>
          <Form.Item {...tailLayout}>
            <Button
              type="primary"
              htmlType="submit"
              size="large"
              loading={loading}
              style={{ width: "50%" }}
            >
              Login
            </Button>
          </Form.Item>
         <Form.Item {...tailLayout}>
         {errMess ? (<Alert
              message={errMess}
              banner
              type="error"
              style={{width: "50%"}}
            />) : ""}
          </Form.Item>
        </Form>
      </Col>
    </Row>
    
  );
}

export default Login;
