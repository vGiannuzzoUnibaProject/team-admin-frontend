
export function exportCSV(data,file){

   let columnDelimiter = ',';
   let lineDelimiter ='\n';

   let keys = Object.keys(data[0]);
   // console.log(typeof keys)
   //delete keys['id']; 
   //delete keys['id_user']; 
  // delete keys['key']; 

   let result = '';
    result += keys.join(columnDelimiter);
    result += lineDelimiter;
    let ctr=0
    data.forEach(function(item) {
        ctr = 0;
        keys.forEach(function(key) {
        if (ctr > 0) result += columnDelimiter;
        
        result += item[key];
        ctr++;
        });
        result += lineDelimiter;
        });
        
        result = 'data:text/csv;charset=utf-8,' + result;
        data = encodeURI(result);
        let link = document.createElement('a');
        link.setAttribute('href', data);
        link.setAttribute('target','_blank');

        link.setAttribute('download', file);
        link.click();

    console.log(result)

}