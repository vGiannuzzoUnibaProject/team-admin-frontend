function getRequest(type,body){
  const token = JSON.parse(localStorage.getItem("user"));
  let userToken
  try {
    userToken = token['token']
    let requestOptions = {
      method: type,
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + userToken
      },
    }

    if (body)
    requestOptions['body']=body

    return requestOptions

  }catch(error){
    return error
  }

}

export {getRequest}