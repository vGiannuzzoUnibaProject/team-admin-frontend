import React, { useEffect } from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { history } from './helpers/history';
import PrivateRoute  from './components/PrivateRoute';
import LoginBackend  from './components/LoginBackendComponent'
import HomeBackend from './components/HomeBackendComponent'
import HomeFrontend from './components/HomeFrontEndComponent'

import TableLog from './components/TableLogComponent'
import TableUsers from './components/TableUsersComponent'
import Actions from './components/ActionsComponent'
import ActionsAdministrator from './components/ActionsAdministrator'

import ChangePassword from './components/ChangePasswordComponent'
import Dashboard from './components/DashboardComponent'
import Error from './components/ErrorComponent'
import Courses from './components/CoursesComponent'
import DetailCourses from './components/DetailCourseComponent'

function App() {
  return (
    <div>
    <Router history={history}>
    <Switch>
        <Route exact path="/" component={HomeFrontend}/>
        <PrivateRoute exact path="/admin/home" View={TableLog}/>
        <PrivateRoute exact path="/admin/logs" View={TableLog}/>
        <PrivateRoute exact path="/admin/dashboard" View={Dashboard}/>
        <PrivateRoute exact path="/admin/users/manage" View={TableUsers}/>
        <PrivateRoute exact path="/admin/actions" View={Actions}/>
        <PrivateRoute exact path="/admin/action_administrators" View={ActionsAdministrator}/>
        <PrivateRoute exact path="/admin/courses" View={Courses}/>
        <PrivateRoute exact path="/admin/courses/id/:id" View={DetailCourses}/>

        <PrivateRoute exact path="/admin/changepassword" View={ChangePassword}/>
        <Route path="/error" component={Error} />


        <Route path="/admin/login" component={LoginBackend} />
        <Route path='*' exact={true} component={HomeFrontend} /> 



    </Switch>
</Router>


</div>
  );
}

export default App;
